package com.wedextim.kafka.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedextim.kafka.entity.Book;
import com.wedextim.kafka.entity.Message;
import com.wedextim.kafka.entity.ReceivedTypes;
import com.wedextim.kafka.entity.User;
import com.wedextim.kafka.producers.Sender;
import com.wedextim.kafka.utils.Caster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InputConsumer {
    @Autowired
    Sender sender;

    private static final Logger LOGGER = LoggerFactory.getLogger(InputConsumer.class);

    ObjectMapper objectMapper;

    @KafkaListener(id = "kafka-batch", topics = {"topic1", "topic2", "topic3"})
    public void receive(
            @Payload List<Message> data
    ) throws ClassNotFoundException {
        objectMapper = new ObjectMapper();
        for (Message item : data) {
            Class<? extends ReceivedTypes> classToCast = Caster.getClass(item.getType());
            if (classToCast.equals(User.class)) {
                User user = objectMapper.convertValue(item.getData(), User.class);
                LOGGER.info("Received message='{}'", user);
                sender.send(user, User.class);
            } else if (classToCast.equals(Book.class)) {
                Book book = objectMapper.convertValue(item.getData(), Book.class);
                LOGGER.info("Received message='{}'", book);
                sender.send(book, Book.class);
            } else {
                LOGGER.info("Incorrect message");
            }
        }
        LOGGER.info("All batch messages received");
    }
}
