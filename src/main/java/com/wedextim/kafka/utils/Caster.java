package com.wedextim.kafka.utils;

import com.wedextim.kafka.entity.Book;
import com.wedextim.kafka.entity.ReceivedTypes;
import com.wedextim.kafka.entity.User;

public class Caster {
    public static Class<? extends ReceivedTypes> getClass(CastClasses tClass) throws ClassNotFoundException {
        switch (tClass) {
            case USER:
                return User.class;
            case BOOK:
                return Book.class;
            default:
                throw new ClassNotFoundException();
        }
    }
}