package com.wedextim.kafka.entity;

import com.wedextim.kafka.utils.CastClasses;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode()
public class Message {
    private CastClasses type;
    private Object data;
}
