package com.wedextim.kafka.entity;

import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class User extends ReceivedTypes {
    private int id;
    private String name;
}
