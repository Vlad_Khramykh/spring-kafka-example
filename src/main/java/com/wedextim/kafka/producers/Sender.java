package com.wedextim.kafka.producers;

import com.wedextim.kafka.entity.Book;
import com.wedextim.kafka.entity.ReceivedTypes;
import com.wedextim.kafka.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class Sender {
    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private KafkaTemplate<String, ReceivedTypes> producer;

    @Value("${kafka.topics.write}")
    private String topic;

    public <T> void send(T data, Class tClass) {
        Message<T> message;
        if (User.class.equals(tClass)) {
            message = MessageBuilder
                    .withPayload(data)
                    .setHeader(KafkaHeaders.TOPIC, topic)
                    .setHeader("EntityClass", User.class)
                    .build();
        } else if (Book.class.equals(tClass)) {
            message = MessageBuilder
                    .withPayload(data)
                    .setHeader(KafkaHeaders.TOPIC, topic)
                    .setHeader("EntityClass", Book.class)
                    .build();
        } else {
            message = MessageBuilder
                    .withPayload(data)
                    .setHeader(KafkaHeaders.TOPIC, topic)
                    .setHeader("EntityClass", com.wedextim.kafka.entity.Message.class)
                    .build();
        }
        LOGGER.info("Sending message='{}' to topic ='{}", data, topic);
        producer.send(message);
    }
}
