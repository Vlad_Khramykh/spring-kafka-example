package com.wedextim.kafka.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedextim.kafka.entity.Book;
import com.wedextim.kafka.entity.Message;
import com.wedextim.kafka.entity.ReceivedTypes;
import com.wedextim.kafka.entity.User;
import com.wedextim.kafka.producers.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OutputConsumer {
    @Autowired
    Sender sender;

    private static final Logger LOGGER = LoggerFactory.getLogger(OutputConsumer.class);

    ObjectMapper objectMapper;

    @KafkaListener(id = "kafka-output", topics = {"topic4"})
    public void receive(
            @Payload List<ReceivedTypes> data,
            @Header("EntityClass") Class tClass
    ) {
        objectMapper = new ObjectMapper();
        for (int i = 0; i < data.size(); i++) {
            if (tClass.equals(User.class)) {
                User user = objectMapper.convertValue(data.get(i), User.class);
                LOGGER.info("Received User (TOPIC-4): '{}'", user);
            } else if (tClass.equals(Book.class)) {
                Book book = objectMapper.convertValue(data.get(i), Book.class);
                LOGGER.info("Received Book (TOPIC-4):'{}'", book);
            } else {
                LOGGER.info("Incorrect header 'EntityClass'");
            }
            // sender.send(data.get(i));
        }
    }
}
