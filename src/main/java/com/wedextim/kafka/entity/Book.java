package com.wedextim.kafka.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class Book extends ReceivedTypes {
    private int code;
    private String name;
}
